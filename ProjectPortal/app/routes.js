﻿(function () {
    angular.module('app')
        .config(['$stateProvider', '$urlRouterProvider', globalConfig]);

    function globalConfig($stateProvider, $urlRouteProvider) {
        $stateProvider
            .state('signin',
            {
                url: '/signin',
                templateUrl: 'app/signin/signin.html',
                controller: 'signinController as vm'
            })
            .state('project',
            {
                url: '/project',
                templateUrl: 'app/project/project.html',
                controller: 'projectController as vm'
            });
        $urlRouteProvider.otherwise('/signin');
    }
})();