﻿(function () {
    angular.module('app').controller('signinController', ['$state', function ($state) {
        var vm = this;
        vm.signInAsNormal = function () {
            $state.go('project');
        };
        vm.signInAsAdmin = function () {
            alert('Not Implemented!');
        };
        vm.signInAsExec = function () {
            alert('Not Implemented!');
        };
    }]);
})();