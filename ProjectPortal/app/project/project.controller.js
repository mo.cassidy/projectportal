﻿(function () {
    angular.module('app').controller('projectController', [projectController]);
    function projectController() {
        var vm = this;
        vm.save = save;
        vm.voteup = voteup;
        vm.votedown = votedown;
        vm.edit = edit;
        vm.add = add;
        vm.cancel = cancel;
        vm.remove = remove;
        vm.filter = filter;
        vm.newProject = {};
        vm.projects = {};
        vm.stages = ['Concept', 'Design', 'Development', 'In Production'];

        activate();

        function activate() {
            vm.projects = [
                {
                    id: 1,
                    name: 'Project Portal',
                    owner: 'Mo Cassidy',
                    createdDate: new Date(2016, 6, 1),
                    website: 'https://gitlab.com/mo.cassidy/projectportal',
                    imageUri: 'http://i1.theportalwiki.net/img/2/29/Achievement_Out_of_the_Blue.jpg',
                    stage: 'Design',
                    tags: [{ text: 'repository' }, { text: 'centralized' }, { text: 'portal' }, { text: 'innovation' }],
                    latestActivty: new Date(2016, 8, 1),
                    upvotes: 12,
                    downvotes: 1,
                    summary: 'You\'re looking at it right now'
                },
                {
                    id: 2,
                    name: 'Universal Search',
                    owner: 'Mo Cassidy',
                    createdDate: new Date(2014, 11, 19),
                    website: '',
                    imageUri: 'http://icons.iconarchive.com/icons/kyo-tux/ginux/64/Start-Menu-Search-icon.png',
                    stage: 'Design',
                    tags: [{ text: 'search' }, { text: 'elastic' }, { text: 'innovation' }],
                    latestActivty: new Date(2015, 8, 20),
                    upvotes: 20,
                    downvotes: 0,
                    summary: 'An application that allows users to navigate the entire Axcess system, and perform any operations, via a simple search interface'
                },
                {
                    id: 1,
                    name: 'K1derful',
                    owner: 'Mo Cassidy',
                    createdDate: new Date(2014, 12, 20),
                    website: '',
                    imageUri: 'http://k1parser.azurewebsites.net/Content/Images/k1derfulicon.gif',
                    stage: 'Design',
                    tags: [{ text: 'parser' }, { text: 'ocr' }, { text: 'tax' }, { text: 'tax-return' }, { text: 'form-k1' }, { text: 'machine-learning' }],
                    latestActivty: new Date(2015, 2, 27),
                    upvotes: 12,
                    downvotes: 7,
                    summary: 'An application to automate the input of additional information from a K1 federal tax return form'
                },
                {
                    id: 4,
                    name: 'Axcess Search',
                    owner: 'Erik Magana',
                    createdDate: new Date(2016, 7, 11),
                    website: 'https://wkaxcess.visualstudio.com/SearchPlatform/',
                    imageUri: 'http://icons.iconarchive.com/icons/kyo-tux/ginux/64/Start-Menu-Search-icon.png',
                    stage: 'Development',
                    tags: [{ text: 'search' }, { text: 'elastic' }, { text: 'document' }, { text: 'portal' }, { text: 'universal-search' }],
                    latestActivty: new Date(2016, 7, 24),
                    upvotes: 1,
                    downvotes: 0
                },
                {
                    id: 7,
                    name: 'Lunar Gs',
                    owner: 'Erik Magana',
                    createdDate: new Date(2016, 5, 12),
                    website: 'http://innovationlab2015.cloudapp.net/CodeGames/Team/Details/52',
                    imageUri: 'http://innovationlab2015.cloudapp.net/CodeGames/Images/Team/00518166-29bb-4ad7-bd76-55b622fad3fd.jpg',
                    stage: 'Concept',
                    tags: [{ text: 'tone-analyzer' }, { text: 'ibm-watson' }, { text: 'real-time-analysis' }, { text: 'chat' }],
                    latestActivty: new Date(2016, 5, 13),
                    upvotes: 6,
                    downvotes: 0,
                    summary: 'Decode emotions from text and offer real time tone analysis. We used IBM Watson\'s Tone Analyzer API and a web Chat application in the Project. Conversation in the chat window is sent to API for analyzing each word / line at a time. Response received from API is represented...'
                },
                {
                    id: 8,
                    name: 'Axcess Pix',
                    owner: 'Joe Taylor',
                    createdDate: new Date(2016, 5, 12),
                    website: 'http://innovationlab2015.cloudapp.net/CodeGames/Team/Details/110',
                    imageUri: 'http://innovationlab2015.cloudapp.net/CodeGames/Images/Member/bd3d81b5-1ef9-49a8-ae7a-5553439b0a25.jpg',
                    stage: 'Development',
                    tags: [{ text: 'tablet' }, { text: 'mobile' }, { text: 'imaging' }, { text: 'receipts' }, { text: 'tax' }],
                    latestActivty: new Date(2016, 5, 13),
                    upvotes: 15,
                    downvotes: 4,
                    summary: 'Snap & Store, Quick Document Capture and Storage / Axcess Pix'
                },
                {
                    id: 9,
                    name: 'Ventripal',
                    owner: 'Mo Cassidy',
                    createdDate: new Date(2016, 5, 12),
                    website: 'https://gitlab.com/ventripal/ventripal',
                    imageUri: 'http://innovationlab2015.cloudapp.net/CodeGames/Images/Team/57495bb3-60d1-45b4-9243-e4702677caf8.png',
                    stage: 'Development',
                    tags: [{ text: 'assistant' }, { text: 'help' }, { text: 'gamification' }, { text: 'collaboration' }],
                    latestActivty: new Date(2016, 5, 13),
                    upvotes: 17,
                    downvotes: 2,
                    summary: 'Ventripal is a friendly, one-stop-shop for working with screens within CCH Axcess. It provides a consistent UI component for getting help, providing feedback and collaborating that\'s familiar across all Axcess screens but has information tailored specifically to each screen.'
                },
                {
                    id: 10,
                    name: 'Axcess Assistant',
                    owner: 'Chris Owen',
                    createdDate: new Date(2016, 5, 12),
                    website: 'https://github.com/codegrunt/CodeGames2016',
                    imageUri: 'http://innovationlab2015.cloudapp.net/CodeGames/Images/Team/1a390cef-ca22-40e4-8fa7-ad147482fd02.png',
                    stage: 'Development',
                    tags: [{ text: 'ai' }, { text: 'machine-learning' }, { text: 'cortana' }],
                    latestActivty: new Date(2016, 5, 13),
                    upvotes: 11,
                    downvotes: 1,
                    summary: 'Axcess virtual assistant'
                }
            ];
            vm.newProject = initializeNewProject();
        }
        function save(newProject) {
            // then add
            vm.projects.push(newProject);
            vm.newProject = initializeNewProject();
            vm.editMode = false;
            vm.addMode = false;
        }
        function initializeNewProject() {
            return {
                id: vm.projects.length,
                name: '',
                owner: 'Mo Cassidy',
                createdDate: Date.now(),
                website: '',
                imageUri: 'http://findicons.com/files/icons/753/gnome_desktop/64/gnome_document_new.png',
                tags: '',
                stage: 'Concept',
                latestActivty: Date.now(),
                upvotes: 0,
                downvotes: 0
            };
        }
        function voteup(project) {
            project.upvotes++;
        }
        function votedown(project) {
            project.downvotes++;
        }
        function edit(project) {
            vm.revertableProject = JSON.parse(JSON.stringify(project));
            vm.newProject = JSON.parse(JSON.stringify(project));
            remove(project);
            vm.editMode = true;
        }
        function add() {
            vm.addMode = true;
            vm.revertableProject = false;
            vm.newProject = initializeNewProject();
        }
        function cancel() {
            if (vm.revertableProject) {
                vm.projects.push(vm.revertableProject);
            }
            vm.addMode = false;
            vm.editMode = false;
            vm.newProject = initializeNewProject();
        }
        function remove(project) {
            vm.projects.forEach(function (match) {
                if (match.id === project.id) {
                    var pos = vm.projects.indexOf(match);
                    vm.projects.splice(pos, 1);
                }
            });
        }
        function filter(text) {
            vm.searchText = text;
        }
    }
})();